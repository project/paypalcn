This module is a sub-module of Commerce Paypal to enable Paypal payment using
RMB (CNY).

Please note that to receive RMB, a separate Paypal China Account is required
which is different from Paypal Global.

Additionally, Paypal China only allows you to receive RMB and not other
currencies.

Dependencies:
Drupal Commerce
Commerce Paypal

Installing Commerce Paypal China:

1. Navigate to /admin/modules . Enable Commerce Paypal China.
2. Navigate to
/admin/commerce/config/payment-methods/manage/commerce_payment_paypal_wps
edit the PayPal WPS and save to update PayPal settings
